//
//  BoardCommentTableViewCell.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 22..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface BoardCommentTableViewCell : UITableViewCell


@property (nonatomic, strong) NSString *comment_id;

@property (nonatomic, strong) IBOutlet UILabel  *nicknameLabel;
@property (nonatomic, strong) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) IBOutlet UILabel  *writeTimeLabel;

@property (nonatomic, strong) IBOutlet UIButton  *likeBtn;



@property (nonatomic, strong) IBOutlet UIButton  *profileBtn;

@property (nonatomic) float requiredCellHeight;

- (IBAction)likeCommentBtnClicked:(id)sender;


@end
