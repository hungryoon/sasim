//
//  AppDelegate.m
//  sasim
//
//  Created by hungryoon on 2014. 5. 24..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    
    rootViewController = [[RootViewController alloc] init];
    self.window.rootViewController = rootViewController;
    /*
     MBAlertView *alert = [MBAlertView alertWithBody:@"Are you sure you want to delete this note? You cannot undo this." cancelTitle:@"Cancel" cancelBlock:nil];
     [alert addButtonWithText:@"Delete" type:MBAlertViewItemTypeDestructive block:^{}];
     [alert setBackgroundAlpha:0.7f];
     [alert addToDisplayQueue];
     */
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark PushNotification


- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
	
    NSString *deviceTokenString;
    const unsigned *tokenBytes = [devToken bytes];
    deviceTokenString = [NSString stringWithFormat:@"%08X%08X%08X%08X%08X%08X%08X%08X",
                       ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                       ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                       ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
	
    
     NSLog(@"Device Token - [%@]", deviceTokenString);
    LMcommon.push_id = deviceTokenString;
    [LMcommon saveUserDefaults];
    // NSLog(@"%@", LMcommon.device_id);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
	/*
	 푸쉬가 오면 실행된다.
	 userInfo에 서버에서 보낸 정보가 담겨있다.
	 NSLog로 확인..(푸쉬가 정상적으로 오면 로그가 보인다.)
	 **/
    
	NSLog(@"didReceiveRemoteNotification - \n %@", userInfo);
    
}


@end
