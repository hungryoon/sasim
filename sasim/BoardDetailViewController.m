//
//  BoardDetailViewController.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 22..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "BoardDetailViewController.h"

#import "SDWebImage/UIButton+WebCache.h"
#import "UIImage+fixOrientation.h"

#define MAX_COMMENT_HEIGHT 9999


@interface BoardDetailViewController ()

@end

@implementation BoardDetailViewController

- (void)addTarget:(id)target action:(SEL)action {
    aTarget = target;
    aSelector = action;
}

- (void)layoutSubviews
{
    CGRect rect;
    NSInteger img_width, img_height;
    
    
    rect = self.photoView.frame;
    rect.origin.y = self.titleTextView.frame.origin.y + self.titleTextView.frame.size.height;
    rect.size.height = 0;

    if (self.photo_1_Btn) {
        img_width = self.photo_1_Btn.imageView.image.size.width;
        img_height = self.photo_1_Btn.imageView.image.size.height;
        if( img_width > rect.size.width){
            img_height = rect.size.width * img_height / img_width;
            img_width = rect.size.width;
        }
        self.photo_1_Btn.frame = CGRectMake(0, rect.size.height, img_width, img_height);
    
        rect.size.height += img_height;
        
        [self.photoView addSubview:self.photo_1_Btn];
    }
    
    if (self.photo_2_Btn) {
        img_width = self.photo_2_Btn.imageView.image.size.width;
        img_height = self.photo_2_Btn.imageView.image.size.height;
        if( img_width > rect.size.width){
            img_height = rect.size.width * img_height / img_width;
            img_width = rect.size.width;
        }
        self.photo_2_Btn.frame = CGRectMake(0, rect.size.height, img_width, img_height);
        rect.size.height += img_height;
        
        [self.photoView addSubview:self.photo_2_Btn];
    }
    
    if (self.photo_3_Btn) {
        img_width = self.photo_3_Btn.imageView.image.size.width;
        img_height = self.photo_3_Btn.imageView.image.size.height;
        if( img_width > rect.size.width){
            img_height = rect.size.width * img_height / img_width;
            img_width = rect.size.width;
        }
        self.photo_3_Btn.frame = CGRectMake(0, rect.size.height, img_width, img_height);
        rect.size.height += img_height;
        
        [self.photoView addSubview:self.photo_3_Btn];
    }
    
    if (self.photo_4_Btn) {
        img_width = self.photo_4_Btn.imageView.image.size.width;
        img_height = self.photo_4_Btn.imageView.image.size.height;
        if( img_width > rect.size.width){
            img_height = rect.size.width * img_height / img_width;
            img_width = rect.size.width;
        }
        self.photo_4_Btn.frame = CGRectMake(0, rect.size.height, img_width, img_height);
        rect.size.height += img_height;
        
        [self.photoView addSubview:self.photo_4_Btn];
    }
    
    self.photoView.frame = rect;
    
    
    rect = self.contentsTextView.frame;
    rect.origin.y = self.photoView.frame.origin.y + self.photoView.frame.size.height;
    self.contentsTextView.frame = rect;
    
    
    rect = self.containerView.frame;
    rect.size.height = 70.0f
                + self.titleTextView.frame.size.height
                + self.photoView.frame.size.height
                + self.contentsTextView.frame.size.height;
    self.containerView.frame = rect;
    
    [self.tableView setTableHeaderView:self.containerView];
    
    
}

#pragma keyboardControl

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect rect = self.tableView.frame;
    rect.size.height = PHONE_SCREEN_HEIGHT - 64.0f -commentField.view.frame.size.height -keyboardBounds.size.height;
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	self.tableView.frame = rect;
    
	
	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
    self.tableView.frame = CGRectMake(0, 64.0f,
                                    PHONE_SCREEN_WIDTH,
                                    PHONE_SCREEN_HEIGHT -64.0f -commentField.view.frame.size.height);
	
	
	// commit animations
	[UIView commitAnimations];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    [self readArticle];
    commentField = [[CommentFieldViewController alloc] initWithNibName:@"CommentFieldViewController" bundle:nil];
    commentField.delegate = self;
    
    [self.view addSubview:commentField.view];
    
    CGRect rect;
    rect = self.tableView.frame;
    rect.size.height = PHONE_SCREEN_HEIGHT -64.0f -commentField.view.frame.size.height;
    self.tableView.frame = rect;
    
    
    
    static NSString *CellIdentifier = @"BoardCommentTableViewCell";
    [self.tableView registerNib:[UINib nibWithNibName:@"BoardCommentTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    self.cellPrototype = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark IBAction

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteBtnClicked:(id)sender
{
    [LMapi api_deleteAricle_member_id:LMcommon.member_id
                              session:LMcommon.session
                           article_id:self.article_id
                               target:self
                               action:@selector(callback_deleteArticle:)];
}




#pragma mark readArticle

- (void)callback_readArticle:(NSDictionary *)result {
    
    NSLog(@"callback_readArticle:%@", result);
    
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        if (LMcommon.member_id!=[dict objectForKey:@"member_id"]) { // 내 글아니면 삭제버튼 제거
            self.deleteBtn.hidden = YES;
        }
        
        
        
        
        self.nicknameLabel.text = [dict objectForKey:@"nickname"];
        self.writeTimeLabel.text = [dict objectForKey:@"date_written"];
        
        NSString *profileImageURL = [NSString stringWithFormat:@"%@%@",
                                     BaseURL,  [dict objectForKey:@"mini_profile_image"]];
        
        [self.profileBtn setImageWithURL:[NSURL URLWithString:profileImageURL]
                                forState:UIControlStateNormal];
        
        
        
        
        
        
        
        
        
        self.titleTextView.text = [dict objectForKey:@"title"];
        [self.titleTextView sizeToFit];
        
        CGRect rect;

        
        
        
        
        if ([[dict objectForKey:@"image1"] length] != 0) {
            
            rect = self.photoView.frame;
            
            self.photo_1_Btn = [[UIButton alloc] init];
            NSString *imgStr = [NSString stringWithFormat:@"%@%@", BaseURL, [dict objectForKey:@"image1"]];
            
            [self.photo_1_Btn setImageWithURL:[NSURL URLWithString:imgStr]
                             forState:UIControlStateNormal
                     placeholderImage:nil
                            completed:
                    ^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                        [self layoutSubviews];
                    }
             
             ];
        
        }
        
        if ([[dict objectForKey:@"image2"] length] != 0) {
            
            rect = self.photoView.frame;
            
            self.photo_2_Btn = [[UIButton alloc] init];
            NSString *imgStr = [NSString stringWithFormat:@"%@%@", BaseURL, [dict objectForKey:@"image2"]];
            
            [self.photo_2_Btn setImageWithURL:[NSURL URLWithString:imgStr]
                                     forState:UIControlStateNormal
                             placeholderImage:nil
                                    completed:
             ^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                 [self layoutSubviews];
             }
             
             ];
            
        }
        
        
        
        if ([[dict objectForKey:@"image3"] length] != 0) {
            
            rect = self.photoView.frame;
            
            self.photo_3_Btn = [[UIButton alloc] init];
            NSString *imgStr = [NSString stringWithFormat:@"%@%@", BaseURL, [dict objectForKey:@"image3"]];
            
            [self.photo_3_Btn setImageWithURL:[NSURL URLWithString:imgStr]
                                     forState:UIControlStateNormal
                             placeholderImage:nil
                                    completed:
             ^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                 [self layoutSubviews];
             }
             
             ];
            
        }
        
        if ([[dict objectForKey:@"image4"] length] != 0) {
            
            rect = self.photoView.frame;
            
            self.photo_4_Btn = [[UIButton alloc] init];
            NSString *imgStr = [NSString stringWithFormat:@"%@%@", BaseURL, [dict objectForKey:@"image4"]];
            
            [self.photo_4_Btn setImageWithURL:[NSURL URLWithString:imgStr]
                                     forState:UIControlStateNormal
                             placeholderImage:nil
                                    completed:
             ^(UIImage *image, NSError *error, SDImageCacheType cacheType){
                 [self layoutSubviews];
             }
             
             ];
            
        }
        
        
        
        
        self.contentsTextView.text = [dict objectForKey:@"contents"];
        [self.contentsTextView sizeToFit];
        
        //self.titleTextView.backgroundColor = [UIColor redColor];
        //self.photoView.backgroundColor = [UIColor cyanColor];
        //self.contentsTextView.backgroundColor = [UIColor blueColor];
        
        

        [self layoutSubviews];

        
        [self loadComment_first];
    }
    
    
}


- (void)callback_loadComment_first:(NSDictionary *)result {
    
    NSLog(@"callback_loadComment_first:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
   
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        NSArray *callbackData = [NSArray arrayWithArray:[dict objectForKey:@"comments"]];
        
        
        
        
        
        self.commentData = [[NSMutableArray alloc] initWithArray:[[callbackData reverseObjectEnumerator] allObjects]];
        [self.tableView reloadData];
        [self.tableView reloadInputViews];

    }
    
}


- (void)readArticle {
    
    
    [self.profileBtn setImage:nil forState:UIControlStateNormal];
    self.nicknameLabel.text = @"";
    
    
    self.writeTimeLabel.text = @"";
    
    self.titleTextView.text = @"";
    self.contentsTextView.text = @"";
    
    
    [LMapi api_readArticle_member_id:LMcommon.member_id
                             session:LMcommon.session
                          article_id:self.article_id
                              target:self
                              action:@selector(callback_readArticle:)];
    
    

    
    
    
    /*
    self.nicknameLabel.text = @"배고픈윤이";
    self.writeTimeLabel.text = @"5분 전";
    self.modifyBtn.hidden = NO;
    
    
    
    self.titleTextView.editable = NO;
    
    self.titleTextView.text = @"유병언 부자 공개수배…신고 보상금 8천만원 유병언 부자 공개수배…신고 보상금 8천만원 유병언 부자 공개수배…신고 보상금 8천만원 ";
    
    self.contentsTextView.text = @"-청구서, 구매한 상품 및 사은품, 경품의 정확한 배송을 위한 활용\n-새로운 상품/서비스나 이벤트 정보의 안내 (개인정보 활용 동의 고객에 한함)\n-고지사항 전달, 본인 의사 확인, 불만 처리, A/S 등 원활한 의사소통 절차에 활용\n";
    */
    
}

- (void)loadComment_first {
    [LMapi api_loadComment_member_id:LMcommon.member_id
                             session:LMcommon.session
                          article_id:self.article_id
                    start_comment_id:nil
                              target:self
                              action:@selector(callback_loadComment_first:)];
}


- (void)callback_deleteComment:(NSDictionary *)result {
    
    NSLog(@"callback_deleteComment:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
        [self.commentData removeObjectAtIndex:selectedRow];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

- (void)callback_deleteArticle:(NSDictionary *)result {
    
    NSLog(@"callback_deleteArticle:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
        [aTarget performSelectorOnMainThread:aSelector withObject:nil waitUntilDone:NO];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}


#pragma mark UIActionSheetDelegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    // ActionSheet가 나올때
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==0) {
        if (isMyComment) {
            
            // 삭제
            [LMapi api_deleteComment_member_id:LMcommon.member_id
                                       session:LMcommon.session
                                    comment_id:selectedCommentId
                                        target:self
                                        action:@selector(callback_deleteComment:)];
            
        }
        else {
            
            // 멘션
            
        }
    }
    if (buttonIndex==1) {
        
        // 복사
        
    }
    
}


#pragma mark - TableView Deleagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict = [self.commentData objectAtIndex:indexPath.row];
    
    self.cellPrototype.nicknameLabel.text = [dict objectForKey:@"nickname"];
    self.cellPrototype.commentTextView.text = [dict objectForKey:@"contents"];
    self.cellPrototype.writeTimeLabel.text = [dict objectForKey:@"date_written"];
    self.cellPrototype.comment_id = [dict objectForKey:@"comment_id"];
    [self.cellPrototype layoutSubviews];
    
    NSLog(@"%f", self.cellPrototype.requiredCellHeight);
    
    return MAX(self.cellPrototype.requiredCellHeight, 65.0f);
    // return [[[self.commentData objectAtIndex:indexPath.row] objectForKey:@"comment_height"] floatValue]+50.0f;
}

-  (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedRow = indexPath.row;
    
    NSDictionary *dict = [self.commentData objectAtIndex:indexPath.row];
    
    selectedCommentId = [dict objectForKey:@"comment_id"];
    
    if ( LMcommon.member_id == [dict objectForKey:@"member_id"] ) { // 내 코멘트
        
        isMyComment = YES;
        
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@""
                                                                delegate:self
                                                       cancelButtonTitle:@"취소"
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:@"삭제", @"복사", nil];
        
        [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
        
    }
    
    else { // 남 코멘트ㅡ
        
        isMyComment = NO;
        selectedMemberId = [dict objectForKey:@"member_id"];
        selectedNickname = [dict objectForKey:@"nickname"];
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@""
                                                                delegate:self
                                                       cancelButtonTitle:@"취소"
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:
                                      [NSString stringWithFormat:@"@%@", selectedNickname],
                                      @"복사", nil];
        
        [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
        
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.commentData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BoardCommentTableViewCell *cell;
    
    cell = (BoardCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BoardCommentTableViewCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BoardCommentTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[BoardCommentTableViewCell class]])
            {
                cell = (BoardCommentTableViewCell *)oneObject;
            }
        }
    }
    NSDictionary *dict = [self.commentData objectAtIndex:indexPath.row];
    
    cell.nicknameLabel.text = [dict objectForKey:@"nickname"];
    cell.commentTextView.text = [dict objectForKey:@"contents"];
    cell.writeTimeLabel.text = [dict objectForKey:@"date_written"];
    cell.comment_id = [dict objectForKey:@"comment_id"];
    
    NSString *profileImageURL = [NSString stringWithFormat:@"%@%@",
                                 BaseURL,  [dict objectForKey:@"mini_profile_image"]];
    [cell.profileBtn setImageWithURL:[NSURL URLWithString:profileImageURL]
                            forState:UIControlStateNormal];
    
    // [cell layoutSubviews];
    // cell.commentTextView.backgroundColor = [UIColor greenColor];
    // cell.writeTimeLabel.backgroundColor = [UIColor redColor];

    
    
    
    //NSLog(@"i%f", cell.writeTimeLabel.frame.origin.y);
    
    
    
    
    // Configure the cell...
    
    return cell;
}




#pragma mark -
#pragma mark CommentField


- (void)callback_writeComment:(NSDictionary *)result {
    
    NSLog(@"callback_writeComment:%@", result);
    
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        [self loadComment_first];
    }
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([commentField.textView isFirstResponder] && [touch view] != commentField.view) {
        [commentField.textView resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void) sendButtonClicked {
    NSLog(@"sendButtonClicked");
    [LMapi api_writeComment_member_id:LMcommon.member_id
                              session:LMcommon.session
                           article_id:self.article_id
                             contents:commentField.textView.text
                        comment_image:commentField.photoImageData
                           mention_id:@""
                               target:self
                               action:@selector(callback_writeComment:)];
    
}

- (void) didChangeHeight {
    
	// get a rect for the textView frame
	CGRect rect = self.tableView.frame;
    rect.size.height = commentField.view.frame.origin.y - 64.0f;
	self.tableView.frame = rect;

}


@end
