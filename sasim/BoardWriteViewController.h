//
//  BoardWriteViewController.h
//  sasim
//
//  Created by hungryoon on 2014. 5. 26..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface BoardWriteViewController : UIViewController < HPGrowingTextViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate >
{
    id aTarget;
    SEL aSelector;
    
    int photoCnt;
}

@property (nonatomic, strong) NSString *board_type;

@property (nonatomic, strong) IBOutlet UIScrollView  *scrollView;
@property (nonatomic, strong) IBOutlet UIView *contentView;

@property (nonatomic, strong) IBOutlet UIButton *profileImageBtn;
@property (nonatomic, strong) IBOutlet UILabel *nicknameLabel;

@property (nonatomic, strong) IBOutlet UIButton *sendBtn;

@property (nonatomic, strong) IBOutlet UITextField *titleTextField;


@property (nonatomic, strong) IBOutlet UIButton *photo_1_Btn;
@property (nonatomic, strong) IBOutlet UIButton *photo_2_Btn;
@property (nonatomic, strong) IBOutlet UIButton *photo_3_Btn;
@property (nonatomic, strong) IBOutlet UIButton *photo_4_Btn;

@property (nonatomic, strong) NSData *photo_1_Data;
@property (nonatomic, strong) NSData *photo_2_Data;
@property (nonatomic, strong) NSData *photo_3_Data;
@property (nonatomic, strong) NSData *photo_4_Data;


@property (nonatomic, strong) NSMutableArray *photoImageArray;
@property (nonatomic, strong) NSMutableArray *photoImageDataArray;

@property (nonatomic, strong) IBOutlet HPGrowingTextView *contentTextView;


- (IBAction)sendBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;

- (void)addTarget:(id)target action:(SEL)action;


@end
