//
//  SecondViewController.h
//  Agreement
//
//  Created by LeeJungWoo on 2014. 5. 22..
//  Copyright (c) 2014년 myro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondAgreeViewController : UIViewController{
    
    NSInteger firstCheckBoxFlag;
    NSInteger secondCheckBoxFlag;

}

@property (weak, nonatomic) IBOutlet UIButton *firstCheckBoxButton;
@property (weak, nonatomic) IBOutlet UIButton *secondCheckBoxButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)checkBoxClicked:(id)sender;

- (IBAction)nextButtonClicked:(id)sender;
- (IBAction)backbuttonClicked:(id)sender;


@end
