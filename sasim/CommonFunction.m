//
//  CommonFunction.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 20..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "CommonFunction.h"
#import "OLGhostAlertView.h"
#import "RootViewController.h"
#import <CommonCrypto/CommonDigest.h>


@implementation CommonFunction



#pragma mark -
#pragma mark singleton


static CommonFunction *_CommonFunction = nil;

+ (CommonFunction*)shared
{
	if( _CommonFunction == nil )
	{
		@synchronized(self)
        {
            if(_CommonFunction == nil)
            {
                _CommonFunction = [[CommonFunction alloc] init];
            }
        }
	}
	
	return _CommonFunction;
}

#pragma mark -
#pragma mark initializer

- (id)init
{
    self = [super init];
    
    if (self)
    {

        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        HUD = nil;
                
        self.isRegistered = [[NSUserDefaults standardUserDefaults] objectForKey:@"isRegistered"];
        
        self.member_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"member_id"];
        self.password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        self.session = [[NSUserDefaults standardUserDefaults] objectForKey:@"session"];
        
        
        self.device_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_id"];
        self.device_type = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_type"];
        self.device_info = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_info"];
        self.push_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"push_id"];
        
        self.student_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"student_id"];
        self.student_dept = [[NSUserDefaults standardUserDefaults] objectForKey:@"student_dept"];
        self.student_campus = [[NSUserDefaults standardUserDefaults] objectForKey:@"student_campus"];

        
        if (self.device_id==nil) {
            self.device_id = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        }
        if (self.device_type==nil) {
            self.device_type = @"I";
        }
        if (self.device_info==nil) {
            self.device_info = [UIDevice currentDevice].systemVersion;
        }
        
        
        
        
    }
    return self;
}

#pragma mark saveUserDefaults

- (void)saveUserDefaults {
    
    [[NSUserDefaults standardUserDefaults] setObject:self.isRegistered forKey:@"isRegistered"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:self.member_id forKey:@"member_id"];
    [[NSUserDefaults standardUserDefaults] setObject:self.password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] setObject:self.session forKey:@"session"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:self.device_id forKey:@"device_id"];
    [[NSUserDefaults standardUserDefaults] setObject:self.device_type forKey:@"device_type"];
    [[NSUserDefaults standardUserDefaults] setObject:self.device_info forKey:@"device_info"];
    [[NSUserDefaults standardUserDefaults] setObject:self.push_id forKey:@"push_id"];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.student_id forKey:@"student_id"];
    [[NSUserDefaults standardUserDefaults] setObject:self.student_name forKey:@"student_name"];
    [[NSUserDefaults standardUserDefaults] setObject:self.student_dept forKey:@"student_dept"];
    [[NSUserDefaults standardUserDefaults] setObject:self.student_campus forKey:@"student_campus"];

}

- (NSString *)calcCertCode:(NSString *)string {
    
    int retInt = 0;
    int day = 0;
    
    for (int i = 0; i < [string length]; i++) {
        retInt += [string characterAtIndex:i]-'0';
    }
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
    day = [components day]; //gives you day
    
    return [NSString stringWithFormat:@"%d", retInt*day];
}

- (int)calcStringBytes:(NSString *)string {
    
    int bytes = 0;
    
    for (int i = 0; i < [string length]; i++) {
        char ch = [string characterAtIndex:i];
        if ('0' <=  ch && ch <= '9') {
            bytes ++;
        }
        else if ('a' <=  ch && ch <= 'z') {
            bytes ++;
        }
        else {
            bytes += 2;
        }
    }
    return bytes;
}

#pragma mark ProgressHUD

- (void)showProgressHUD:(NSString *)msg {
    
    if (HUD!=nil) [HUD removeFromSuperview];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.appDelegate.window.rootViewController.view];
	
    [self.appDelegate.window.rootViewController.view addSubview:HUD];

    [HUD setAnimationType:MBProgressHUDAnimationZoomIn];
    [HUD setLabelText:msg];
    
	[HUD show:YES];
}

- (void)hideProgressHUD {
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark MsgView

- (void)showMsgView:(NSString *)msg
{
    OLGhostAlertView *alertView;
    
    alertView = [[OLGhostAlertView alloc] init];
    
    
    [alertView setTitle:msg];
    [alertView setMessage:nil];
    
    [alertView setPosition:OLGhostAlertViewPositionBottom];
    [alertView setStyle:OLGhostAlertViewStyleDark];
    [alertView setTimeout:2.0f];
    [alertView setDismissible:YES];
    
    
    [alertView show];
}

#pragma mark aboutRootViewController

- (void)changeMenu:(int)menu
{
    RootViewController *vc = (RootViewController *)self.appDelegate.window.rootViewController;
    [vc changeMenu:menu];
}

#pragma mark SHA1

- (NSString *)sha1:(NSString *)input
{
    CC_SHA1_CTX ctx;
    
    CC_SHA1_Init(&ctx);
    
    
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1_Update(&ctx, data.bytes, data.length);
    
    
    NSString *hashString = @"temptation";
    const char *chstr = [hashString cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *hashData = [NSData dataWithBytes:chstr length:hashString.length];
    
    CC_SHA1_Update(&ctx, hashData.bytes, hashData.length);
    CC_SHA1_Final(digest, &ctx);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    
    // NSLog(@"%@", output);
    
    return output;
}


@end
