//
//  apiLibrary.m
//  
//
//  Created by hungryoon on 2014.5.5....
//
//  Copyright 2014. All rights reserved.
//

#import "ApiLibrary.h"
#import "PostOperation.h"

@implementation ApiLibrary


#pragma mark - 
#pragma mark singleton

static ApiLibrary *_ApiLibrary = nil;

+ (ApiLibrary*)shared
{
	if( _ApiLibrary == nil )
	{
		@synchronized(self)
        {
            if(_ApiLibrary == nil)
            {
                _ApiLibrary = [[ApiLibrary alloc] init];

            }
        }		
	}
	
	return _ApiLibrary;
}

#pragma mark - 
#pragma mark initializer

- (id)init
{
    self = [super init];
    if (self)
    {
        self.operationQueue = [[NSOperationQueue alloc]init];
        [_operationQueue setMaxConcurrentOperationCount:3];
    }
    return self;
}

#pragma mark -
#pragma mark request

- (void)requestWithUrlString:(NSString *)url
                         dic:(NSDictionary *)dic
                      target:(id)target
                      action:(SEL)selector
{
    
    PostOperation *op = [[PostOperation alloc] initWithUrlString:url
                                                             dic:dic
                                                          target:target
                                                          action:selector];
    
    [_operationQueue addOperation:op];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"requestURLNotificationStart"
                                                        object:nil userInfo:nil];
}


#pragma mark -
#pragma mark APIs


- (void)api_test
{
    /*
     
    UIImage *image = [UIImage imageNamed:@"yoon_bw.jpg"];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8f);
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"0d0e9f9e2823a0e9d9c9",@"device_id",
                         @"I",@"device_type",
                         @"iOS7.1.1/iPhone5s",@"device_info",
                         @"2009001839",@"student_no",
                         @"최윤",@"student_name",
                         @"융합전자공학부",@"student_dept",
                         @"0d0e9f9e2823a0e9d9c9",@"push_id",
                         @"704",@"cert_code",
                         //imageData,@"image",
                         nil];
    */
    /*
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"2009001839",@"student_no",
                         @"1234",@"password",
                         @"0d0e9f9e2823a0e9d9c9",@"device_id",
                         //imageData,@"image",
                         nil];
    
    
    
    PostOperation *op = [[PostOperation alloc]initWithUrlString:[self makeURL:@"memberData/register/"]
                                                            dic:dic
                                                         target:self
                                                         action:nil];
    [self.operationQueue addOperation:op];
     */
    
}

#pragma mark memberData


- (void)api_register_device_id:(NSString *)device_id
                   device_type:(NSString *)device_type
                   device_info:(NSString *)device_info
                    student_id:(NSString *)student_id
                  student_name:(NSString *)student_name
                  student_dept:(NSString *)student_dept
                student_campus:(NSString *)student_campus
                       push_id:(NSString *)push_id
                        target:(id)target
                        action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/memberData/register/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:device_id forKey:@"device_id"];
    [dic setValue:device_type forKey:@"device_type"];
    [dic setValue:device_info forKey:@"device_info"];
    
    [dic setValue:student_id forKey:@"student_id"];
    [dic setValue:student_name forKey:@"student_name"];
    [dic setValue:student_dept forKey:@"student_dept"];
    [dic setValue:student_campus forKey:@"student_campus"];
    
    
    [dic setValue:push_id forKey:@"push_id"];
    [dic setValue:[LMcommon sha1:student_id] forKey:@"cert_code"];
    //[dic setValue:@"896" forKey:@"cert_code"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
}

- (void)api_login_member_id:(NSString *)member_id
                    password:(NSString *)password
                      target:(id)target
                      action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/memberData/login/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:password] forKey:@"password"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}

- (void)api_changeNickname_member_id:(NSString *)member_id
                            password:(NSString *)password
                            nickname:(NSString *)nickname
                              target:(id)target
                              action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/memberData/changeNickname/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:password] forKey:@"password"];
    [dic setValue:nickname forKey:@"nickname"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}

- (void)api_changeProfileImage_member_id:(NSString *)member_id
                                password:(NSString *)password
                           profile_image:(NSData *)profile_image
                      profile_image_mini:(NSData *)profile_image_mini
                      profile_image_micro:(NSData *)profile_image_micro
                              target:(id)target
                              action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/memberData/changeProfileImage/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:password] forKey:@"password"];
    [dic setValue:profile_image forKey:@"profile_image"];
    [dic setValue:profile_image_mini forKey:@"profile_image_mini"];
    [dic setValue:profile_image_micro forKey:@"profile_image_micro"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}

#pragma mark board

- (void)api_loadArticle_member_id:(NSString *)member_id
                          session:(NSString *)session
                       board_type:(NSString *)board_type
                 start_article_id:(NSString *)start_article_id
                           target:(id)target
                           action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/loadArticle/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:board_type forKey:@"board_type"];
    [dic setValue:start_article_id forKey:@"start_article_id"];
    
    
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}


- (void)api_readArticle_member_id:(NSString *)member_id
                          session:(NSString *)session
                       article_id:(NSString *)article_id
                           target:(id)target
                           action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/readArticle/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:article_id forKey:@"article_id"];
    
    
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}

- (void)api_writeArticle_member_id:(NSString *)member_id
                           session:(NSString *)session
                        board_type:(NSString *)board_type
                             title:(NSString *)title
                          contents:(NSString *)contents
                     images_number:(NSString *)images_number
                           image1:(NSData *)image1
                           image2:(NSData *)image2
                           image3:(NSData *)image3
                           image4:(NSData *)image4
                            target:(id)target
                            action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/writeArticle/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    
    [dic setValue:board_type forKey:@"board_type"];
    [dic setValue:title forKey:@"title"];
    [dic setValue:contents forKey:@"contents"];
    [dic setValue:images_number forKey:@"images_number"];
    [dic setValue:image1 forKey:@"image1"];
    [dic setValue:image2 forKey:@"image2"];
    [dic setValue:image3 forKey:@"image3"];
    [dic setValue:image4 forKey:@"image4"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}

- (void)api_deleteAricle_member_id:(NSString *)member_id
                           session:(NSString *)session
                        article_id:(NSString *)article_id
                            target:(id)target
                            action:(SEL)selector
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/deleteArticle/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:article_id forKey:@"article_id"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
}








- (void)api_loadComment_member_id:(NSString *)member_id
                          session:(NSString *)session
                       article_id:(NSString *)article_id
                 start_comment_id:(NSString *)start_comment_id
                           target:(id)target
                           action:(SEL)selector
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/loadComment/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:article_id forKey:@"article_id"];
    [dic setValue:start_comment_id forKey:@"start_comment_id"];
    
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
    
}


- (void)api_writeComment_member_id:(NSString *)member_id
                           session:(NSString *)session
                        article_id:(NSString *)article_id
                          contents:(NSString *)contents
                     comment_image:(NSData *)comment_image
                        mention_id:(NSString *)mention_id
                            target:(id)target
                            action:(SEL)selector
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/writeComment/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:article_id forKey:@"article_id"];
    [dic setValue:contents forKey:@"contents"];
    [dic setValue:comment_image forKey:@"comment_image"];
    [dic setValue:mention_id forKey:@"mention_id"];
    
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
}

- (void)api_deleteComment_member_id:(NSString *)member_id
                            session:(NSString *)session
                         comment_id:(NSString *)comment_id
                             target:(id)target
                             action:(SEL)selector
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/deleteComment/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:comment_id forKey:@"comment_id"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
}




- (void)api_likeComment_member_id:(NSString *)member_id
                          session:(NSString *)session
                       comment_id:(NSString *)comment_id
                           target:(id)target
                           action:(SEL)selector
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BaseURL, @"/board/likeComment/"];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setValue:member_id forKey:@"member_id"];
    [dic setValue:[LMcommon sha1:session] forKey:@"session"];
    [dic setValue:comment_id forKey:@"comment_id"];
    
    [self requestWithUrlString:url
                           dic:dic
                        target:target
                        action:selector];
}







@end
