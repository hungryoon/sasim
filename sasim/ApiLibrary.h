//
//  apiLibrary.m
//
//
//  Created by hungryoon on 2014.5.5....
//
//  Copyright 2014. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostOperation.h"
#import <UIKit/UIKit.h>

@interface ApiLibrary : NSObject {
}

@property (nonatomic, strong) NSOperationQueue *operationQueue;

/*
 Singleton
*/

#define LMapi [ApiLibrary shared]

+ (ApiLibrary *)shared;

- (void)api_test;

// memberData

- (void)api_register_device_id:(NSString *)device_id
                   device_type:(NSString *)device_type
                   device_info:(NSString *)device_info
                    student_id:(NSString *)student_id
                  student_name:(NSString *)student_name
                  student_dept:(NSString *)student_dept
                student_campus:(NSString *)student_campus
                       push_id:(NSString *)push_id
                        target:(id)target
                        action:(SEL)selector;

- (void)api_login_member_id:(NSString *)member_id
                   password:(NSString *)password
                     target:(id)target
                     action:(SEL)selector;

- (void)api_changeNickname_member_id:(NSString *)member_id
                            password:(NSString *)password
                            nickname:(NSString *)nickname
                              target:(id)target
                              action:(SEL)selector;

- (void)api_changeProfileImage_member_id:(NSString *)member_id
                                password:(NSString *)password
                           profile_image:(NSData *)profile_image
                      profile_image_mini:(NSData *)profile_image_mini
                     profile_image_micro:(NSData *)profile_image_micro
                                  target:(id)target
                                  action:(SEL)selector;




// board


//// article

- (void)api_loadArticle_member_id:(NSString *)member_id
                          session:(NSString *)session
                       board_type:(NSString *)board_type
                 start_article_id:(NSString *)start_article_id
                           target:(id)target
                           action:(SEL)selector;

- (void)api_readArticle_member_id:(NSString *)member_id
                          session:(NSString *)session
                       article_id:(NSString *)article_id
                           target:(id)target
                           action:(SEL)selector;

- (void)api_writeArticle_member_id:(NSString *)member_id
                           session:(NSString *)session
                        board_type:(NSString *)board_type
                             title:(NSString *)title
                          contents:(NSString *)contents
                     images_number:(NSString *)images_number
                            image1:(NSData *)image1
                            image2:(NSData *)image2
                            image3:(NSData *)image3
                            image4:(NSData *)image4
                            target:(id)target
                            action:(SEL)selector;

- (void)api_deleteAricle_member_id:(NSString *)member_id
                           session:(NSString *)session
                        article_id:(NSString *)article_id
                            target:(id)target
                            action:(SEL)selector;

//// comment

- (void)api_loadComment_member_id:(NSString *)member_id
                          session:(NSString *)session
                       article_id:(NSString *)article_id
                 start_comment_id:(NSString *)start_comment_id
                           target:(id)target
                           action:(SEL)selector;

- (void)api_writeComment_member_id:(NSString *)member_id
                           session:(NSString *)session
                        article_id:(NSString *)article_id
                          contents:(NSString *)contents
                     comment_image:(NSData *)comment_image
                        mention_id:(NSString *)mention_id
                            target:(id)target
                            action:(SEL)selector;

- (void)api_deleteComment_member_id:(NSString *)member_id
                            session:(NSString *)session
                         comment_id:(NSString *)comment_id
                             target:(id)target
                             action:(SEL)selector;

- (void)api_likeComment_member_id:(NSString *)member_id
                          session:(NSString *)session
                       comment_id:(NSString *)comment_id
                           target:(id)target
                           action:(SEL)selector;


@end
