//
//  CommonFunction.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 20..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"



#define LMcommon [CommonFunction shared]


@interface CommonFunction : NSObject
{
    
    MBProgressHUD *HUD;
    
}

@property (nonatomic) AppDelegate *appDelegate;

@property (nonatomic) NSString *isRegistered;
@property (nonatomic) NSString *member_id;
@property (nonatomic) NSString *password;

@property (nonatomic) NSString *session;

@property (nonatomic) NSString *device_id;
@property (nonatomic) NSString *device_type;
@property (nonatomic) NSString *device_info;
@property (nonatomic) NSString *push_id;

@property (nonatomic) NSString *student_id;
@property (nonatomic) NSString *student_name;
@property (nonatomic) NSString *student_dept;
@property (nonatomic) NSString *student_campus;


@property (nonatomic) NSString *nickname;
@property (nonatomic) NSString *profile_url;
@property (nonatomic) NSString *profile_mini_url;
@property (nonatomic) NSString *profile_micro_url;



+ (CommonFunction *)shared;


- (void)showMsgView:(NSString *)msg;
- (void)showProgressHUD:(NSString *)msg;
- (void)hideProgressHUD;

- (void)changeMenu:(int)menu;
- (NSString *)sha1:(NSString *)input;

- (void)saveUserDefaults;
- (NSString *)calcCertCode:(NSString *)string;
- (int)calcStringBytes:(NSString *)string;


@end
