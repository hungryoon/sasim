//
//  AuthProcessViewController.h
//  AuthProcess
//
//  Created by hungryoon on 2014. 4. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthProcessViewController : UIViewController <UIAlertViewDelegate, UIWebViewDelegate, UIScrollViewDelegate>

{
    NSURLRequest *requestObj;
    NSString *student_id;
    NSString *student_name;
    NSString *student_dept;
}

@property (nonatomic, strong) IBOutlet UIWebView *webView;

- (IBAction)backbuttonclk:(id)sender;

@end
