//
//  HmdMainViewController.h
//  HmdMenu
//
//  Created by hungryoon on 2014. 5. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HmdMainViewController : UIViewController


@property (nonatomic, strong) IBOutlet UITableView  *tableView;

@property (nonatomic, strong) IBOutlet UIButton  *toggleBtn;

@end
