//
//  HmdMainViewController.m
//  HmdMenu
//
//  Created by hungryoon on 2014. 5. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "HmdMainViewController.h"
#import "HmdListTableViewCell.h"
#import "SWRevealViewController.h"
#import "SVPullToRefresh.h"

#import "UIImageView+WebCache.h"

@interface HmdMainViewController ()

@end

@implementation HmdMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SWRevealViewController *revealController = self.revealViewController;
    [self.toggleBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.tableView addPullToRefreshWithActionHandler:^{}];
    [self.tableView addInfiniteScrollingWithActionHandler:^{}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HmdListTableViewCell *cell;
    
    cell = (HmdListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HmdListTableViewCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HmdListTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[HmdListTableViewCell class]])
            {
                cell = (HmdListTableViewCell *)oneObject;
            }
        }
    }
    
    
    
    // Configure the cell...
    
    return cell;
}



@end
