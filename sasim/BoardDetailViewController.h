//
//  BoardDetailViewController.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 22..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BoardCommentTableViewCell.h"
#import "CommentFieldViewController.h"

@interface BoardDetailViewController : UIViewController <CommentFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate>

{
    
    id aTarget;
    SEL aSelector;
    
    
    CommentFieldViewController *commentField;
    
    BOOL isMyComment;
    NSInteger selectedRow;
    NSString *selectedCommentId;
    NSString *selectedMemberId;
    NSString *selectedNickname;
    
}

@property (nonatomic, strong) NSString *article_id;

@property (nonatomic, strong) NSMutableArray  *commentData;



@property (nonatomic, strong) IBOutlet UITableView  *tableView;


@property (nonatomic, strong) IBOutlet UIView *containerView;


@property (nonatomic, strong) IBOutlet UIButton *profileBtn;
@property (nonatomic, strong) IBOutlet UILabel *nicknameLabel;

@property (nonatomic, strong) IBOutlet UILabel *writeTimeLabel;
@property (nonatomic, strong) IBOutlet UIButton *deleteBtn;


@property (nonatomic, strong) IBOutlet UITextView *titleTextView;
@property (nonatomic, strong) IBOutlet UIView *photoView;
@property (nonatomic, strong) IBOutlet UITextView *contentsTextView;



@property (nonatomic, strong) UIButton *photo_1_Btn;
@property (nonatomic, strong) UIButton *photo_2_Btn;
@property (nonatomic, strong) UIButton *photo_3_Btn;
@property (nonatomic, strong) UIButton *photo_4_Btn;


@property (strong) BoardCommentTableViewCell *cellPrototype;

- (IBAction)deleteBtnClicked:(id)sender;
- (IBAction)backBtnClicked:(id)sender;


- (void)addTarget:(id)target action:(SEL)action;
- (void)readArticle;


@end
