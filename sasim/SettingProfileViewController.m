//
//  SettingProfileViewController.m
//  sasim
//
//  Created by hungryoon on 2014. 5. 24..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "SettingProfileViewController.h"

#import "RootViewController.h"

@interface SettingProfileViewController ()

@end

@implementation SettingProfileViewController

- (void) scalingUploadImage
{
    int maxSize = 740;
    
    //가로 비율 변경
    NSInteger img_width = self.profileImage.size.width;
    NSInteger img_height = self.profileImage.size.height;
    if( img_width > maxSize){
        img_height = maxSize * img_height / img_width;
        img_width = maxSize;
    }
    else if(img_height > maxSize){
        img_width = maxSize * img_width / img_height;
        img_height = maxSize;
    }
    
    //이미지 깨지는것 방지
    UIGraphicsBeginImageContext( CGSizeMake(img_width, img_height) );
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, img_height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0, 0, img_width, img_height), [self.profileImage CGImage]);
    
    //변경된 이미지
    UIImage *scaledUploadImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.profileImageData = [[NSMutableData alloc] initWithData:UIImageJPEGRepresentation(scaledUploadImage, 0.8f)];
    
    
    maxSize = 110;
    
    //가로 비율 변경
    img_width = self.profileImage.size.width;
    img_height = self.profileImage.size.height;
    if( img_width > maxSize){
        img_height = maxSize * img_height / img_width;
        img_width = maxSize;
    }
    else if(img_height > maxSize){
        img_width = maxSize * img_width / img_height;
        img_height = maxSize;
    }
    
    //이미지 깨지는것 방지
    UIGraphicsBeginImageContext( CGSizeMake(img_width, img_height) );
    context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, img_height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0, 0, img_width, img_height), [self.profileImage CGImage]);
    
    //변경된 이미지
    scaledUploadImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.profileImageMiniData = [[NSMutableData alloc] initWithData:UIImageJPEGRepresentation(scaledUploadImage, 0.8f)];
    
    
    maxSize = 34;
    
    //가로 비율 변경
    img_width = self.profileImage.size.width;
    img_height = self.profileImage.size.height;
    if( img_width > maxSize){
        img_height = maxSize * img_height / img_width;
        img_width = maxSize;
    }
    else if(img_height > maxSize){
        img_width = maxSize * img_width / img_height;
        img_height = maxSize;
    }
    
    //이미지 깨지는것 방지
    UIGraphicsBeginImageContext( CGSizeMake(img_width, img_height) );
    context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, img_height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0, 0, img_width, img_height), [self.profileImage CGImage]);
    
    //변경된 이미지
    scaledUploadImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.profileImageMicroData = [[NSMutableData alloc] initWithData:UIImageJPEGRepresentation(scaledUploadImage, 0.8f)];
    
}

- (void)callback_changeProfileImage:(NSMutableDictionary *)result {
    
    NSLog(@"callback_changeNickname:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^{}];
        [(RootViewController *)LMcommon.appDelegate.window.rootViewController loadMainView];
    }
    
}

- (void)callback_changeNickname:(NSMutableDictionary *)result {
    
    NSLog(@"callback_changeNickname:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        [LMapi api_changeProfileImage_member_id:LMcommon.member_id
                                       password:LMcommon.password
                                  profile_image:self.profileImageData
                             profile_image_mini:self.profileImageMiniData
                            profile_image_micro:self.profileImageMicroData
                                         target:self
                                         action:@selector(callback_changeProfileImage:)];
        
    }
    
}

- (IBAction)confirmBtnClicked:(id)sender {
    [LMapi api_changeNickname_member_id:LMcommon.member_id
                               password:LMcommon.password
                               nickname:self.nicknameTextField.text
                                 target:self
                                 action:@selector(callback_changeNickname:)];
}

#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    return newLength <= 6 || returnKey;

}

#pragma mark Photo

- (IBAction)profileImageBtnClicked:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@""
                                                            delegate:self
                                                   cancelButtonTitle:@"취소"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"사진촬영",
                                  @"앨범에서 사진 선택", @"삭제", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)imagePickerForCamera
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera ;
	
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
	
}

- (void)imagePickerForLibrary
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = (id)self;
	picker.allowsEditing = YES;
	
    [self presentViewController:picker animated:YES completion:nil];
	
}

#pragma mark UIActionSheet Delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    // ActionSheet가 나올때
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        // 사진촬영
    {
        [self imagePickerForCamera];
    }
    else if (buttonIndex == 1)
        // 라이브러리에서 선택
    {
        [self imagePickerForLibrary];
    }
    
    else {
        
        [self.profileImageBtn setImage:nil forState:UIControlStateNormal];
        self.profileImage = nil;
        
    }
}

#pragma mark UIImagePickerController Delegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage  *image;
    if ([info objectForKey:@"UIImagePickerControllerEditedImage"]) {
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerEditedImage"];
    }else{
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];

    [self.profileImageBtn setImage:image forState:UIControlStateNormal];
    self.profileImage = image;
    [self scalingUploadImage];
    
    
}

#pragma mark -


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.nicknameTextField isFirstResponder] && [touch view] != self.nicknameTextField) {
        [self.nicknameTextField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
