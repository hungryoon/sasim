//
//  BoardCommentTableViewCell.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 22..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "BoardCommentTableViewCell.h"


@implementation BoardCommentTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)layoutSubviews
{
    
    //[super layoutSubviews];
    CGRect rect;
    CGSize size;
    
    
    NSLog(@"%@", self.commentTextView.text);
    
    // [self.commentTextView sizeToFit];
    
    rect = self.commentTextView.frame;
    size = [self.commentTextView sizeThatFits:CGSizeMake(rect.size.width, 9999.0f)];
    rect.size.height = size.height;
    self.commentTextView.frame = rect;
    
    // NSLog(@"%f", self.commentTextView.frame.size.width);
    // self.commentTextView.backgroundColor = [UIColor greenColor];

    [self.writeTimeLabel sizeToFit];
    
    rect = self.writeTimeLabel.frame;
    rect.origin.y = self.commentTextView.frame.origin.y + self.commentTextView.frame.size.height;
    rect.size.height = 20.0f;
    self.writeTimeLabel.frame = rect;
    
    
    
    rect = self.likeBtn.frame;
    rect.origin.y = self.writeTimeLabel.frame.origin.y;
    rect.origin.x = self.writeTimeLabel.frame.origin.x + self.writeTimeLabel.frame.size.width;
    rect.size.height = 20.0f;
    self.likeBtn.frame = rect;
    
    //self.writeTimeLabel.backgroundColor = [UIColor greenColor];
    //self.likeBtn.backgroundColor = [UIColor grayColor];
    // NSLog(@"%f", self.requiredCellHeight);
    self.requiredCellHeight = self.writeTimeLabel.frame.origin.y + self.writeTimeLabel.frame.size.height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)callback_likeComment:(NSDictionary *)result {
    
    NSLog(@"callback_likeComment:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
    }
    
}


- (IBAction)likeCommentBtnClicked:(id)sender {
    [LMapi api_likeComment_member_id:LMcommon.member_id
                             session:LMcommon.session
                          comment_id:self.comment_id
                              target:self
                              action:@selector(callback_likeComment:)];
}

@end
