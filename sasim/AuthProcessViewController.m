//
//  AuthProcessViewController.m
//  AuthProcess
//
//  Created by hungryoon on 2014. 4. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "AuthProcessViewController.h"
#import "SettingProfileViewController.h"

#define LOGIN_URL [NSURL URLWithString:@"https://m.hanyang.ac.kr/login.page"]
#define INFO_URL [NSURL URLWithString:@"https://m.hanyang.ac.kr/haksa/hsjb/hsjb001001.page?apiUrl=%2FHASA%2FA201300005.json&apiUrlPath=%2FHASA%2FA201300005.json&apiUrlParam=&menu_id=M008247&requestType=page"]


@interface AuthProcessViewController ()

@end

@implementation AuthProcessViewController

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString *titleString = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString *htmlString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    
    NSLog(@"%@", htmlString);
    NSLog(@"%@", titleString);
    
    if ([titleString rangeOfString:@"로그인"].location != NSNotFound ) {
        // '로그인'창 일 때
    }
    else if ( [titleString rangeOfString:@"CAMPUS"].location != NSNotFound ) {
        // "CAMPUS'가 포함 될 때
        
        if ([htmlString rangeOfString:@"로그아웃"].location != NSNotFound) {
            // 로그인 성공
            
            [self goToInfoPage];
        }
        else {
            [self goToLoginPage];
        }
    }
    else if ( [titleString rangeOfString:@"에러"].location != NSNotFound ) {
        [self goToLoginPage];
    }
    else if ( [titleString rangeOfString:@"기본정보및연락처"].location != NSNotFound ) {
        
        [self getInfo:htmlString];

    }
    else {
        [self goToLoginPage];
    }
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.webView;
}

- (void)goToLoginPage {
    requestObj = [NSURLRequest requestWithURL:LOGIN_URL];
    [self.webView loadRequest:requestObj];
}

- (void)goToInfoPage {
    requestObj = [NSURLRequest requestWithURL:INFO_URL];
    [self.webView loadRequest:requestObj];
}

- (void)getInfo:(NSString *)htmlString {
    
    //사용자 학번 추출
    NSArray *userNumberSeperation = [htmlString componentsSeparatedByString:@"<div class=\"ui-block-b  ui-txt-right  par25\" id=\"hakbunNm\"><p class=\"ui-link-inherit ui-li-desc\"><strong>"];
    NSArray *userNumber = [[userNumberSeperation objectAtIndex:1] componentsSeparatedByString:@"</strong></p></div></div></h3><p class=\"ui-li-desc\">"];

    //사용자 이름 추출
    NSArray *userNameSeperation = [htmlString componentsSeparatedByString:@"<div class=\"ui-block-b  ui-txt-right  par25\" id=\"userNm\"><p class=\"ui-link-inherit ui-li-desc\"><strong>"];
    NSArray *userName = [[userNameSeperation objectAtIndex:1] componentsSeparatedByString:@"</strong></p></div></div></h3><p class=\"ui-li-desc\">"];
    
    //사용자 학과 추출
    NSArray *userDepSeperation = [htmlString componentsSeparatedByString:@"<div class=\"ui-block-b  ui-txt-right  par25\" id=\"jeongongNm\"><p class=\"ui-link-inherit ui-li-desc\"><strong>"];
    NSArray *userDep = [[userDepSeperation objectAtIndex:1] componentsSeparatedByString:@"</strong></p></div></div></h3><p class=\"ui-li-desc\">"];
    
    
    student_id = [userNumber objectAtIndex:0];
    student_name = [userName objectAtIndex:0];
    student_dept = [userDep objectAtIndex:0];
    
    
    
    LMcommon.student_id = student_id;
    LMcommon.student_name = student_name;
    LMcommon.student_dept = student_dept;
    
    
    //정보사용 동의 경고창
    NSString *alertMessage = [NSString stringWithFormat:@"%@, %@, %@, 사심에서 귀하의 정보를 수집하는 것에 동의하십니까?", student_id, student_name, student_dept];
    UIAlertView *alertView;
    alertView = [[UIAlertView alloc]initWithTitle:@"로그인 감지"
                                          message:alertMessage
                                         delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"아니오",@"예",nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==1) {
        
        //LMcommon.student_name = @"Choi Yoon";
        //LMcommon.student_dept = @"Elec";
        // 안녕
        
        [LMcommon showProgressHUD:@"사심서버 인증 중..."];
        [LMapi api_register_device_id:LMcommon.device_id
                          device_type:LMcommon.device_type
                          device_info:LMcommon.device_info
                           student_id:LMcommon.student_id
                         student_name:LMcommon.student_name
                         student_dept:LMcommon.student_dept
                       student_campus:LMcommon.student_campus
                              push_id:LMcommon.push_id
                               target:self
                               action:@selector(callback_register:)];
    }
}




- (void)callback_register:(NSMutableDictionary *)result {
    
    NSLog(@"callback_Register:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
        [LMcommon hideProgressHUD];
        LMcommon.isRegistered = @"YES";
        LMcommon.member_id = [dict objectForKey:@"member_id"];
        LMcommon.password = [dict objectForKey:@"password"];
        [LMcommon saveUserDefaults];
    
        
        SettingProfileViewController *vc = [[SettingProfileViewController alloc] initWithNibName:@"SettingProfileViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    self.webView.scrollView.bounces = NO;
    //self.webView.scrollView.scrollEnabled = NO;
    //정우야안녕
    
    [self goToLoginPage];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backbuttonclk:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
