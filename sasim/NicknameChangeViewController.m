//
//  NicknameChangeViewController.m
//  sasim
//
//  Created by hungryoon on 2014. 5. 24..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "NicknameChangeViewController.h"

#import "UIViewController+MJPopupViewController.h"
@interface NicknameChangeViewController ()

@end

@implementation NicknameChangeViewController


- (void)callback_changeNickname:(NSMutableDictionary *)result {
    
    NSLog(@"callback_changeNickname:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
        [LMcommon.appDelegate.window.rootViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex==0) return;
    
    [LMapi api_changeNickname_member_id:LMcommon.member_id
                                password:LMcommon.password
                               nickname:self.nicknameTextField.text
                                 target:self
                                 action:@selector(callback_changeNickname:)];
    
}


- (IBAction)nicknameChangeBtnClicked:(id)sender {
    
    [self.nicknameTextField resignFirstResponder];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"닉넴변경?" delegate:self cancelButtonTitle:@"아뇽" otherButtonTitles:@"넴",nil];
    
    [alertView show];
    
}

#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    
    return newLength <= 6 || returnKey;
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
