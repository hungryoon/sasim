//
//  BoardListTableViewCell.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 21..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardListTableViewCell : UITableViewCell

/*
@property (nonatomic, strong) NSString  *article_id;
@property (nonatomic, strong) NSString  *profile_image;
*/


@property (nonatomic, strong) IBOutlet UILabel  *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel  *nicknameLabel;
@property (nonatomic, strong) IBOutlet UILabel  *hitCountLabel;
@property (nonatomic, strong) IBOutlet UILabel  *writtenTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel  *commentNumberLabel;


@property (nonatomic, strong) IBOutlet UIButton  *profileBtn;



@end
