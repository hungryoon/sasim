//
//  SideMenuViewController.m
//  SideMenu
//
//  Created by hungryoon on 2014. 4. 23..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SWRevealViewController.h"
#import "UIViewController+MJPopupViewController.h"

#import "ProfileImageViewController.h"
#import "NicknameChangeViewController.h"

#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)reloadData {
    NSURL *imageURL = [NSURL URLWithString:LMcommon.profile_mini_url];
    
    [self.profileImageBtn setImageWithURL:imageURL
                                 forState:UIControlStateNormal
                         placeholderImage:[UIImage imageNamed:@"no_profile_img.png"]];
    
    if (LMcommon.nickname==nil) self.nicknameLabel.text = @"";
    else                        self.nicknameLabel.text = LMcommon.nickname;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Profile Change Method

- (IBAction)profileImageBtnClicked:(id)sender {
    ProfileImageViewController *vc = [[ProfileImageViewController alloc] initWithNibName:@"ProfileImageViewController" bundle:nil];
    [LMcommon.appDelegate.window.rootViewController presentPopupViewController:vc animationType:MJPopupViewAnimationFade];
    
}

- (IBAction)nicknameChangeBtnClicked:(id)sender {
    NicknameChangeViewController *vc = [[NicknameChangeViewController alloc] initWithNibName:@"NicknameChangeViewController" bundle:nil];
    [LMcommon.appDelegate.window.rootViewController presentPopupViewController:vc animationType:MJPopupViewAnimationFade];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *text = nil;
    switch ( indexPath.row )
    {
        case 0: text = @"sidemenu_hmd_btn.png"; break;
        case 1: text = @"sidemenu_seoul_btn.png"; break;
        case 2: text = @"sidemenu_erica_btn.png"; break;
        case 3: text = @"sidemenu_global_btn.png"; break;
        case 4: text = @"sidemenu_love_btn.png"; break;
        case 5: text = @"sidemenu_market_btn.png"; break;
    }
    
    
    cell.imageView.image = [UIImage imageNamed:text];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
    [imgView setImage:[UIImage imageNamed:text]];
    [cell addSubview:imgView];
    //cell.textLabel.text = text;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = self.tableView.backgroundColor;
    
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = self.revealViewController;
    
    NSInteger row = indexPath.row;
    
    if ( row == _previouslySelectedRow )
    {
        [revealController revealToggleAnimated:YES];
        return;
    }
    
    _previouslySelectedRow = row;
    
    [LMcommon changeMenu:row];
}

@end
