//
//  RootViewController.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 20..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

#import "SideMenuViewController.h"

#import "WelcomeViewController.h"
#import "HmdMainViewController.h"
#import "BoardMainViewController.h"

@interface RootViewController : UIViewController <SWRevealViewControllerDelegate>
{
    
    SWRevealViewController *mainRevealVC;
    
    SideMenuViewController *sideMenuVC;
    
    HmdMainViewController *hmdMainVC;
    UINavigationController *hmdNaviCon;
    
    BoardMainViewController *seoulVC;
    UINavigationController *seoulNaviCon;
    
    BoardMainViewController *ericaVC;
    UINavigationController *ericaNaviCon;
    
    
    BoardMainViewController *globalVC;
    UINavigationController *globalNaviCon;
    
    
    BoardMainViewController *marketVC;
    UINavigationController *marketNaviCon;
    
    BoardMainViewController *loveVC;
    UINavigationController *loveNaviCon;
    
    
    UIView *statusBarView;
    
}

- (void)changeMenu:(int)menu;
- (void)loginProcess;
- (void)loadMainView;

@end
