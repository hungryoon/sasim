//
//  SideMenuViewController.h
//  SideMenu
//
//  Created by hungryoon on 2014. 4. 23..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger _previouslySelectedRow;
}

@property (nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *profileImageBtn;
@property (strong, nonatomic) IBOutlet UIButton *nicknameChangeBtn;

@property (strong, nonatomic) IBOutlet UILabel *nicknameLabel;



- (void)reloadData;

- (IBAction)profileImageBtnClicked:(id)sender;
- (IBAction)nicknameChangeBtnClicked:(id)sender;

@end
