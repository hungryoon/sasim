//
//  NicknameChangeViewController.h
//  sasim
//
//  Created by hungryoon on 2014. 5. 24..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NicknameChangeViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *nicknameTextField;


- (IBAction)nicknameChangeBtnClicked:(id)sender;


@end
