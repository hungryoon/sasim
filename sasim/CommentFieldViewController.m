//
//  CommentFieldViewController.m
//  CommentField
//
//  Created by hungryoon on 2014. 5. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "CommentFieldViewController.h"

#define COMMENT_PHOTO_HEIGHT_LIMIT 60.0f
#define COMMENT_PHOTO_WIDTH_LIMIT  200.0f
#define PHOTO_PADDING_X 5.0f
#define PHOTO_PADDING_Y 5.0f


@interface CommentFieldViewController ()

@end

@implementation CommentFieldViewController

- (void) scalingUploadImage
{
    int maxSize = 740;
    
    //가로 비율 변경
    NSInteger img_width = self.photoImage.size.width;
    NSInteger img_height = self.photoImage.size.height;
    if( img_width > maxSize){
        img_height = maxSize * img_height / img_width;
        img_width = maxSize;
    }
    else if(img_height > maxSize){
        img_width = maxSize * img_width / img_height;
        img_height = maxSize;
    }
    
    //이미지 깨지는것 방지
    UIGraphicsBeginImageContext( CGSizeMake(img_width, img_height) );
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, img_height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0, 0, img_width, img_height), [self.photoImage CGImage]);
    
    //변경된 이미지
    UIImage *scaledUploadImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.photoImageData = [[NSMutableData alloc] initWithData:UIImageJPEGRepresentation(scaledUploadImage, 0.8f)];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
        
        self.photoImage = nil;
        self.photoImageView = nil;
        
        imageHeight = 0.0f;
        imageWidth = 0.0f;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, PHONE_SCREEN_HEIGHT-self.view.frame.size.height,
                                 PHONE_SCREEN_WIDTH, self.view.frame.size.height);
    
    
	self.textView.minNumberOfLines = 1;
	self.textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
	self.textView.returnKeyType = UIReturnKeyGo; //just as an example
	self.textView.delegate = self;
    self.textView.keyboardType = UIKeyboardTypeDefault;
    self.textView.returnKeyType = UIReturnKeyDefault;
    // Do any additional setup after loading the view from its nib.
    
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma HPGrowingTextViewDelegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
   // float diff = (height - self.view.frame.size.height);
    NSLog(@"%f", height);
    
	CGRect frame;
    
    float commentFieldHeight = height + 9.0f;
    
    frame = self.view.frame;
    frame.origin.y -= commentFieldHeight - frame.size.height;
    frame.size.height = commentFieldHeight;
    if (imageHeight > 0) {
        frame.origin.y -= imageHeight + 2*PHOTO_PADDING_Y;
        frame.size.height += imageHeight + 2*PHOTO_PADDING_Y;
    }
	self.view.frame = frame;
    
    
    frame = self.textView.frame;
    if (imageHeight > 0)    frame.origin.y = imageHeight + 2*PHOTO_PADDING_Y + 3.0f;
    else                    frame.origin.y = 3.0f;
    frame.size.height = commentFieldHeight;
    self.textView.frame = frame;

}

- (void)growingTextView:(HPGrowingTextView *)growingTextView didChangeHeight:(float)height
{
    [self.delegate didChangeHeight];
}


#pragma keyboardControl

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = self.view.frame;
    containerFrame.origin.y -= keyboardBounds.size.height;
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	self.view.frame = containerFrame;
    
	
	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	

	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
    self.view.frame = CGRectMake(0, PHONE_SCREEN_HEIGHT-self.view.frame.size.height,
                                 PHONE_SCREEN_WIDTH, self.view.frame.size.height);
	
	
	// commit animations
	[UIView commitAnimations];
}

#pragma mark Photo

- (void)showPhotoActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@""
                                                            delegate:self
                                                   cancelButtonTitle:@"취소"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"사진찍기",
                                  @"앨범에서 골라봐", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)imagePickerForCamera
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera ;
	
    // picker.allowsEditing = YES;
    [(RootViewController *)LMcommon.appDelegate.window.rootViewController
     presentViewController:picker animated:YES completion:nil];
	
}

- (void)imagePickerForLibrary
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = (id)self;
	// picker.allowsEditing = YES;
	
    [(RootViewController *)LMcommon.appDelegate.window.rootViewController
     presentViewController:picker animated:YES completion:nil];
	
}

#pragma mark UIActionSheet Delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    // ActionSheet가 나올때
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        // 사진촬영
    {
        [self imagePickerForCamera];
    }
    else if (buttonIndex == 1)
        // 라이브러리에서 선택
    {
        [self imagePickerForLibrary];
    }
    
    [self.textView resignFirstResponder];
}

#pragma mark UIImagePickerController Delegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage  *image;
    if ([info objectForKey:@"UIImagePickerControllerEditedImage"]) {
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerEditedImage"];
    }else{
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    }
    
    [self inputPhoto:image];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
	
}

#pragma mark Photo

-(void)inputPhoto:(UIImage *)image {
    
    CGRect rect;
    
    
    rect = self.textView.frame;
    
    self.photoContainerView = [[UIView alloc]
                                initWithFrame:CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, COMMENT_PHOTO_HEIGHT_LIMIT+2*PHOTO_PADDING_Y)];
    self.photoContainerView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.photoContainerView];
    
    
    
    imageHeight = image.size.height;
    imageWidth = image.size.width;
    
    if ( imageHeight <= COMMENT_PHOTO_HEIGHT_LIMIT && imageWidth <= COMMENT_PHOTO_WIDTH_LIMIT ) {
        
    }
    else {
        imageHeight = COMMENT_PHOTO_HEIGHT_LIMIT;
        imageWidth = COMMENT_PHOTO_HEIGHT_LIMIT * (image.size.width / image.size.height);
        
        if ( imageWidth > COMMENT_PHOTO_WIDTH_LIMIT ) {
            imageWidth = COMMENT_PHOTO_WIDTH_LIMIT;
            imageHeight  = COMMENT_PHOTO_WIDTH_LIMIT * (image.size.height / image.size.width);
        }
    }
    
    
    self.photoImageView = [[UIImageView alloc]
                           initWithFrame:CGRectMake(PHOTO_PADDING_X, PHOTO_PADDING_Y, imageWidth, imageHeight)];
    [self.photoImageView setImage:image];
    self.photoImage = image;
    [self.photoContainerView addSubview:self.photoImageView];
    
    rect = self.view.frame;
    rect.origin.y -= imageHeight + 2*PHOTO_PADDING_Y;
    rect.size.height += imageHeight + 2*PHOTO_PADDING_Y;
    self.view.frame = rect;
    
    
    
    rect = self.photoContainerView.frame;
    rect.size.height = imageHeight+2*PHOTO_PADDING_Y;
    self.photoContainerView.frame = rect;
    
    
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(imageWidth, PHOTO_PADDING_Y, 15, 15)];
    btn.backgroundColor = [UIColor redColor];
    [btn addTarget:self action:@selector(deletePhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [self.photoContainerView addSubview:btn];
    
   
    [self.delegate didChangeHeight];
   
}

-(void)deletePhoto {
    CGRect rect;

    

    
    self.photoImage = nil;
    [self.photoContainerView removeFromSuperview];
    [self.photoImageView removeFromSuperview];
    
    

    

    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	
	// set views with new info

    rect = self.view.frame;
    rect.origin.y += imageHeight + 2*PHOTO_PADDING_Y;
    rect.size.height -= imageHeight + 2*PHOTO_PADDING_Y;
    self.view.frame = rect;
    
	
	// commit animations
	[UIView commitAnimations];
    
    
    imageHeight = 0.0f;
    imageWidth  = 0.0f;
}



#pragma mark IBAction

- (IBAction)sendButtonClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(sendButtonClicked)]) {
        [self.textView resignFirstResponder];
        [self.delegate sendButtonClicked];
    }
}

- (IBAction)photoButtonClicked:(id)sender {
    [self showPhotoActionSheet];
}

@end
