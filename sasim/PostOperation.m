//
//  PostOperation.m
//  project_M
//
//  Created by kang inchan on 11. 10. 26..
//  Copyright (c) 2011 macbugi. All rights reserved.
//

#import "PostOperation.h"

@implementation PostOperation

@synthesize dataDict;
@synthesize aURLString;
@synthesize receiveData;

- (id)initWithUrlString:(NSString *)urlString     
                    dic:(NSDictionary *)dic 
                 target:(id)target 
                 action:(SEL)selector
{
    self = [super init];
    if (self) {
        
        aTarget = target;
        aselector = selector;
        self.dataDict = dic;
        self.aURLString = urlString;
        self.receiveData = [[NSMutableData alloc]init];
       
            
    }
    return self;
}

- (void)main {
	
    @autoreleasepool
    
    {
        
        // UTF8 String Encoding
        NSString *urlString = [aURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:urlString];

        NSString *boundary = @"0xKhTmLbOuNdArY";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary]; 	
        NSMutableData *body = [[NSMutableData alloc]init];

        // dataDic의 값들을 bodyString에 넣는다.
        NSArray *keys = [dataDict allKeys];
        
        //[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding] ];
        
        for (int i = 0; i < [keys count]; i++) 
        
        {
            NSString *key = [keys objectAtIndex:i];
            
            if (!([key isEqualToString:@"image1"] ||
                  [key isEqualToString:@"image2"] ||
                  [key isEqualToString:@"image3"] ||
                  [key isEqualToString:@"image4"] ||
                  [key isEqualToString:@"profile_image"] ||
                  [key isEqualToString:@"profile_image_mini"] ||
                  [key isEqualToString:@"profile_image_micro"]
                  )) {
                // 일반적인 경우
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding] ];
                NSString *value = [dataDict objectForKey:key];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key,value] dataUsingEncoding:NSUTF8StringEncoding] ];
            }
            
            else { //이미지
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding] ];
                
                NSData *imageData = [dataDict objectForKey:key];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, [NSString stringWithFormat:@"image_%d.jpg", i]] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                //NSLog(@"%@", imageData);
                [body appendData:[NSData dataWithData:imageData]];
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            
            
        }
        
        NSLog(@"body: %@",[[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding]);
        /*
         requset setting
         **/
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:2.0f];
        [request setHTTPMethod:@"POST"];
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:body];
        
        //NSLog(@"%@", request);
        // NSLog(@"%@", body);
                
        NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
        
        
        
        while(!connectionFinished)
            /* 
             connection의 delegate 함수가 실행될떄까지 루프를 돌린다.
             **/
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        
        
        
        if (connection) 
            /*
             연결이 성공하면.. 
             **/
        {
            //NSLog(@"receiveData: %@",self.receiveData);
            NSMutableString *receiveString = [[NSMutableString alloc]initWithData:self.receiveData encoding:NSUTF8StringEncoding];
            if (receiveString == nil) {
                receiveString =[[NSMutableString alloc] initWithData:self.receiveData encoding:NSASCIIStringEncoding];
                //NSLog(@"temp: %@",temp);
                //[receiveString setString:temp];  
            }
            NSLog(@"receiveString : %@",receiveString);
            
            
            NSError *error;
            NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self.receiveData
                                   options:NSJSONReadingMutableContainers error:&error];
            

            // NSLog(@"%@", receiveString);
            //NSLog(@"dict : %@",jsonDict);
             // 결과 출력
            /*
            for (NSString *key in jsonDict) {
                 NSLog(@"Key: %@, Value: %@", key, [jsonDict valueForKey:key]);
            }
            */

            
            if ([[NSThread currentThread] isCancelled] == NO) 
            
            {
                
                if (aTarget==nil || aselector==nil) {
                    NSLog(@"Target or selector is missing!");
                }
                else {
                    [aTarget performSelectorOnMainThread:aselector withObject:jsonDict waitUntilDone:NO];
                }
            }
            
        }
        
    }
    
}

#pragma mark - 
#pragma mark NSURLConnectionDelegate

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    connectionFinished = TRUE;
    
    [self performSelectorOnMainThread:@selector(hideActivityIndicator) withObject:nil waitUntilDone:0.0];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"requestURLNotificationFinish"
                                                        object:nil userInfo:nil];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.receiveData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"connection error : %@",error);
    [self performSelectorOnMainThread:@selector(hideActivityIndicator) withObject:nil waitUntilDone:0.3];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"requestURLNotificationFinish"
                                                        object:nil userInfo:nil];
    [LMcommon showMsgView:@"서버연결에 실패했습니다. \n잠시 후 다시 시도해주세요!"];
    /*
    [IapiLibrary performSelectorOnMainThread:@selector(showAlertView:)
                                  withObject:@"Connection Error." 
                               waitUntilDone:YES];
    */
    connectionFinished = TRUE;
}

- (void) hideActivityIndicator
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
@end
