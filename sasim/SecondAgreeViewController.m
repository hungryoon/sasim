//
//  SecondViewController.m
//  Agreement
//
//  Created by LeeJungWoo on 2014. 5. 22..
//  Copyright (c) 2014년 myro. All rights reserved.
//

#import "SecondAgreeViewController.h"
#import "AuthProcessViewController.h"

#import "SettingProfileViewController.h"

@interface SecondAgreeViewController ()

@end

@implementation SecondAgreeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nextButton.alpha = 0.0f;
    firstCheckBoxFlag = 0;
    secondCheckBoxFlag = 0;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkBoxClicked:(id)sender {
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    
    if (sender == self.firstCheckBoxButton) {
        
        if (firstCheckBoxFlag) {
            firstCheckBoxFlag = 0;
            self.firstCheckBoxButton.backgroundColor = [UIColor whiteColor];
        }
        else {
            firstCheckBoxFlag = 1;
            self.firstCheckBoxButton.backgroundColor = [UIColor blueColor];
        }
        
    }
    else {
        
        if (secondCheckBoxFlag) {
            secondCheckBoxFlag = 0;
            self.secondCheckBoxButton.backgroundColor = [UIColor whiteColor];
        }
        else {
            secondCheckBoxFlag = 1;
            self.secondCheckBoxButton.backgroundColor = [UIColor blueColor];
        }
        
    }
    
    if (firstCheckBoxFlag && secondCheckBoxFlag)    self.nextButton.alpha = 1.0f;
    else                                            self.nextButton.alpha = 0.0f;
    
    [UIView commitAnimations];
    
}

- (IBAction)nextButtonClicked:(id)sender {

    AuthProcessViewController *vc = [[AuthProcessViewController alloc] initWithNibName:@"AuthProcessViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];

  /*
    SettingProfileViewController *vc = [[SettingProfileViewController alloc] initWithNibName:@"SettingProfileViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
*/
}

- (IBAction)backbuttonClicked:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];

}

@end
