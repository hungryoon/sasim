//
//  SettingProfileViewController.h
//  sasim
//
//  Created by hungryoon on 2014. 5. 24..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingProfileViewController : UIViewController <UIActionSheetDelegate, UITextFieldDelegate>


@property (strong, nonatomic) UIImage *profileImage;
@property (strong, nonatomic) NSData *profileImageData;
@property (strong, nonatomic) NSData *profileImageMiniData;
@property (strong, nonatomic) NSData *profileImageMicroData;

@property (strong, nonatomic) IBOutlet UIButton *profileImageBtn;
@property (strong, nonatomic) IBOutlet UITextField *nicknameTextField;


- (IBAction)profileImageBtnClicked:(id)sender;
- (IBAction)confirmBtnClicked:(id)sender;


@end
