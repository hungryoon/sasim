//
//  RootViewController.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 20..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "RootViewController.h"
#import "BoardWriteViewController.h"

#import "SDImageCache.h"


@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        hmdMainVC = nil;
        hmdNaviCon = nil;
        
        seoulVC = nil;
        seoulNaviCon = nil;
        
        ericaVC = nil;
        ericaNaviCon = nil;
        
        marketVC = nil;
        marketNaviCon = nil;
    }
    return self;
}


- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    
    if (position==FrontViewPositionLeft) {
        
        NSLog(@"Left!");

        statusBarView.alpha = 0.0f;
        
    }
    else if (position==FrontViewPositionRight) {
       
        NSLog(@"Right!");
        
        statusBarView.alpha = 1.0f;
        
    }
    else {
        
        NSLog(@"blah!");
        statusBarView.alpha = 0.0f;
    
    }
    
    [UIView commitAnimations];
    
}

- (void)revealController:(SWRevealViewController *)revealController panGestureMovedToLocation:(CGFloat)location progress:(CGFloat)progress {
    float alpha = progress*1.3;
    if (alpha > 1.0f) alpha = 1.0f;
    statusBarView.alpha = alpha;
}



- (void)callback_login:(NSMutableDictionary *)result {
    
    NSLog(@"callback_login:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
        
        LMcommon.session = [dict objectForKey:@"session"];
        LMcommon.profile_url = [NSString stringWithFormat:@"%@%@",
                                        BaseURL, [dict objectForKey:@"profile_image"]];
        LMcommon.profile_mini_url = [NSString stringWithFormat:@"%@%@",
                                BaseURL, [dict objectForKey:@"mini_profile_image"]];
        
        LMcommon.profile_micro_url = [NSString stringWithFormat:@"%@%@",
                                BaseURL, [dict objectForKey:@"micro_profile_image"]];
        
        
        LMcommon.nickname = [dict objectForKey:@"nickname"];
        //if (LMcommon.nickname==(id)[NSNull null]) LMcommon.nickname = nil;
        
        
        
        [LMcommon saveUserDefaults];
        
        [sideMenuVC reloadData];
        
    }
    
}

- (void)loginProcess {
    
    [LMapi api_login_member_id:LMcommon.member_id
                       password:LMcommon.password
                         target:self
                         action:@selector(callback_login:)];

}


- (void)loadWelcomeView {
    
    WelcomeViewController *vc = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:nc animated:YES completion:nil];
    
    NSLog(@"%@", nc);
    NSLog(@"%@", vc);
    NSLog(@"%@", self);
    /*
    LMcommon.push_id = @"simulator";
    LMcommon.student_id = @"2009001839";
    LMcommon.student_name = @"최윤";
    LMcommon.student_dept = @"융합전자공학부";
    LMcommon.student_campus = @"S";
    [LMcommon saveUserDefaults];
    */

}


- (void)loadMainView {
    
    NSLog(@"loadMainView");
    
    
    hmdMainVC = [[HmdMainViewController alloc] initWithNibName:@"HmdMainViewController" bundle:nil];
    hmdNaviCon = [[UINavigationController alloc] initWithRootViewController:hmdMainVC];
    hmdNaviCon.navigationBarHidden = YES;
    
    sideMenuVC = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
    
    
    mainRevealVC = [[SWRevealViewController alloc]
                    initWithRearViewController:sideMenuVC frontViewController:hmdNaviCon];
    
    mainRevealVC.delegate = self;
    mainRevealVC.rearViewRevealWidth = 250;
    mainRevealVC.bounceBackOnOverdraw = NO;
    mainRevealVC.stableDragOnOverdraw = NO;
    mainRevealVC.rearViewRevealOverdraw = NO;
    mainRevealVC.frontViewShadowOpacity = 0.0f;
    
    
    
    mainRevealVC.view.backgroundColor = [UIColor blackColor];
    sideMenuVC.view.backgroundColor = [UIColor blackColor];
    
    
    [self.view addSubview:mainRevealVC.view];
    
    statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    statusBarView.backgroundColor = [UIColor blackColor];
    statusBarView.alpha = 0.0f;
    [self.view addSubview:statusBarView];
    
    
    [self loginProcess];
}

- (void)changeMenu:(int)menu {
    switch (menu) {
        case 0: // 한마당
            
            if (hmdMainVC==nil) {
                
                hmdMainVC = [[HmdMainViewController alloc] initWithNibName:@"HmdMainViewController" bundle:nil];
                hmdNaviCon = [[UINavigationController alloc] initWithRootViewController:hmdMainVC];
                hmdNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:hmdNaviCon animated:YES];
            
            
            break;

        case 1: // 자유게시판 서울
            
            if (seoulVC==nil) {
                
                seoulVC = [[BoardMainViewController alloc] initWithNibName:@"BoardMainViewController" bundle:nil];
                seoulVC.board_type = @"seoul";
                [seoulVC.naviTitleBtn setImage:[UIImage imageNamed:@"common_seoul_navi.png"]
                                      forState:UIControlStateNormal];
                seoulNaviCon = [[UINavigationController alloc] initWithRootViewController:seoulVC];
                seoulNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:seoulNaviCon animated:YES];
            
            break;
            
            

        case 2: // 자유게시판 에리카
            
            if (ericaVC==nil) {
                
                ericaVC = [[BoardMainViewController alloc] initWithNibName:@"BoardMainViewController" bundle:nil];
                ericaVC.board_type = @"erica";
                [ericaVC.naviTitleBtn setImage:[UIImage imageNamed:@"common_erica_navi.png"]
                                      forState:UIControlStateNormal];
                ericaNaviCon = [[UINavigationController alloc] initWithRootViewController:ericaVC];
                ericaNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:ericaNaviCon animated:YES];
            
            break;
            
            
        case 3: // 자유게시판 글로벌
            
            if (globalVC==nil) {
                
                globalVC = [[BoardMainViewController alloc] initWithNibName:@"BoardMainViewController" bundle:nil];
                globalVC.board_type = @"global";
                [globalVC.naviTitleBtn setImage:[UIImage imageNamed:@"common_global_navi.png"]
                                      forState:UIControlStateNormal];
                globalNaviCon = [[UINavigationController alloc] initWithRootViewController:globalVC];
                globalNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:globalNaviCon animated:YES];
            
            break;
            
        case 4: // 사랑의실천
            
            if (loveVC==nil) {
                
                loveVC = [[BoardMainViewController alloc] initWithNibName:@"BoardMainViewController" bundle:nil];
                loveVC.board_type = @"love";
                [loveVC.naviTitleBtn setImage:[UIImage imageNamed:@"common_love_navi.png"]
                                      forState:UIControlStateNormal];
                loveNaviCon = [[UINavigationController alloc] initWithRootViewController:loveVC];
                loveNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:loveNaviCon animated:YES];
            
            break;


        case 5: // 중고장터
            
            if (marketVC==nil) {
                
                marketVC = [[BoardMainViewController alloc] initWithNibName:@"BoardMainViewController" bundle:nil];
                marketVC.board_type = @"market";
                [marketVC.naviTitleBtn setImage:[UIImage imageNamed:@"common_market_navi.png"]
                                      forState:UIControlStateNormal];
                marketNaviCon = [[UINavigationController alloc] initWithRootViewController:marketVC];
                marketNaviCon.navigationBarHidden = YES;
                
            }
            
            [mainRevealVC pushFrontViewController:marketNaviCon animated:YES];
            
            break;
            

            
        default:
            break;
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[SDImageCache sharedImageCache] clearDisk];
    // self.view.backgroundColor = [UIColor darkGrayColor];
    // Do any additional setup after loading the view.
    
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PHONE_SCREEN_WIDTH, PHONE_SCREEN_HEIGHT)];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    NSLog(@"%@", LMcommon.isRegistered);
    if ([LMcommon.isRegistered isEqualToString:@"YES"]) {
        [self loadMainView];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    
    if ( ! [LMcommon.isRegistered isEqualToString:@"YES"] ) {
        [self loadWelcomeView];
    }
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isRegistered"];

    
    //[LMcommon showMsgViewWithTitle:@"닉네임은 최대 한글6자까지 가능합니다." message:nil];
    //[LMcommon showMsgView:@"닉네임은 최대 한글6자까지 가능합니다."];
    //[LMcommon showProgressHUD:@"Loading"];
    //[LMapi api_test];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
