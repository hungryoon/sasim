//
//  WelcomeViewController.m
//  AuthProcess
//
//  Created by LeeJungWoo on 2014. 5. 22..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "WelcomeViewController.h"
#import "FirstAgreeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)WelcomeButtonclk:(id)sender {
    FirstAgreeViewController *vc = [[FirstAgreeViewController alloc] initWithNibName:@"FirstAgreeViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
