//
//  BoardListTableViewCell.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 21..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "BoardListTableViewCell.h"

@implementation BoardListTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
