//
//  HmdListTableViewCell.h
//  HmdMenu
//
//  Created by hungryoon on 2014. 5. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HmdListTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UIImage *contentsImage;
@property (nonatomic, strong) IBOutlet UILabel *contentsLabel;


@end
