//
//  BoardWriteViewController.m
//  sasim
//
//  Created by hungryoon on 2014. 5. 26..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "BoardWriteViewController.h"

//#import "BoardMainViewController.m"
#import "SDWebImage/UIButton+WebCache.h"
#import "UIImage+fixOrientation.h"

@interface BoardWriteViewController ()

@end

@implementation BoardWriteViewController


- (void)addTarget:(id)target action:(SEL)action {
    aTarget = target;
    aSelector = action;
}


#pragma mark Photo

- (void)showPhotoActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@""
                                                            delegate:self
                                                   cancelButtonTitle:@"취소"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"사진찍기",
                                  @"앨범에서 사진 선택", nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)imagePickerForCamera
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera ;
	
    // picker.allowsEditing = YES;
    
    [(RootViewController *)LMcommon.appDelegate.window.rootViewController
     presentViewController:picker animated:YES completion:nil];
	
}

- (void)imagePickerForLibrary
{
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = (id)self;
	// picker.allowsEditing = YES;
	
    [(RootViewController *)LMcommon.appDelegate.window.rootViewController
     presentViewController:picker animated:YES completion:nil];
	
}

#pragma mark UIActionSheet Delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    // ActionSheet가 나올때
    [self.titleTextField resignFirstResponder];
    [self.contentTextView resignFirstResponder];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        // 사진촬영
    {
        [self imagePickerForCamera];
    }
    else if (buttonIndex == 1)
        // 라이브러리에서 선택
    {
        [self imagePickerForLibrary];
    }
    
    
}

#pragma mark UIImagePickerController Delegate

- (void)setPhotoBtn
{
    if ([self.photoImageArray count] > 0) {
        [self.photo_1_Btn setImage:[self.photoImageArray objectAtIndex:0] forState:UIControlStateNormal];
    }
    if ([self.photoImageArray count] > 1) {
        [self.photo_2_Btn setImage:[self.photoImageArray objectAtIndex:1] forState:UIControlStateNormal];
    }
    if ([self.photoImageArray count] > 2) {
        [self.photo_3_Btn setImage:[self.photoImageArray objectAtIndex:2] forState:UIControlStateNormal];
    }
    if ([self.photoImageArray count] > 3) {
        [self.photo_4_Btn setImage:[self.photoImageArray objectAtIndex:3] forState:UIControlStateNormal];
    }
}

-(void)setPhotoData
{
    int maxSize = 1440;
    
    for (int i = 0; i < [self.photoImageArray count]; i++) {
        
        UIImage *image = [[self.photoImageArray objectAtIndex:i] fixOrientation];
        
        //가로 비율 변경
        NSInteger img_width = image.size.width;
        NSInteger img_height = image.size.height;
        if( img_width > maxSize){
            img_height = maxSize * img_height / img_width;
            img_width = maxSize;
        }
        else if(img_height > maxSize){
            img_width = maxSize * img_width / img_height;
            img_height = maxSize;
        }
        
        //이미지 깨지는것 방지
        UIGraphicsBeginImageContext( CGSizeMake(img_width, img_height) );
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM(context, 0.0, img_height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawImage(context, CGRectMake(0, 0, img_width, img_height), [image CGImage]);
        
        //변경된 이미지
        UIImage *scaledUploadImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSMutableData *imageData = [[NSMutableData alloc] initWithData:UIImageJPEGRepresentation(scaledUploadImage, 0.8f)];
        
        [self.photoImageDataArray setObject:imageData atIndexedSubscript:i];
    }
    
    
    self.photo_1_Data = nil;
    self.photo_2_Data = nil;
    self.photo_3_Data = nil;
    self.photo_4_Data = nil;
    
    if ([self.photoImageDataArray count] > 0) {
        self.photo_1_Data = [self.photoImageDataArray objectAtIndex:0];
    }
    if ([self.photoImageDataArray count] > 1) {
        self.photo_2_Data = [self.photoImageDataArray objectAtIndex:1];
    }
    if ([self.photoImageDataArray count] > 2) {
        self.photo_3_Data = [self.photoImageDataArray objectAtIndex:2];
    }
    if ([self.photoImageDataArray count] > 3) {
        self.photo_4_Data = [self.photoImageDataArray objectAtIndex:3];
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage  *image;
    if ([info objectForKey:@"UIImagePickerControllerEditedImage"]) {
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerEditedImage"];
    }else{
        image = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    }
    
    // [self inputPhoto:image];
    [self.photoImageArray addObject:image];
    [self setPhotoBtn];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
	
}

- (IBAction)photoBtnClicked:(id)sender
{
    [self showPhotoActionSheet];
}


- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)callback_writeArticle:(NSMutableDictionary *)result {
    
    NSLog(@"callback_writeArticle:%@",result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        
        
        [aTarget performSelectorOnMainThread:aSelector withObject:nil waitUntilDone:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex==0) return;
    [self setPhotoData];
    [LMapi api_writeArticle_member_id:LMcommon.member_id
                              session:LMcommon.session
                           board_type:self.board_type
                                title:self.titleTextField.text
                             contents:self.contentTextView.text
                        images_number:[NSString stringWithFormat:@"%d", [self.photoImageArray count]]
                              image1:self.photo_1_Data
                              image2:self.photo_2_Data
                              image3:self.photo_3_Data
                              image4:self.photo_4_Data
                               target:self
                               action:@selector(callback_writeArticle:)];
    
}

- (IBAction)sendBtnClicked:(id)sender {
    
    [self.contentTextView resignFirstResponder];
    [self.titleTextField resignFirstResponder];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"글등록?" delegate:self cancelButtonTitle:@"아뇽" otherButtonTitles:@"넴",nil];
    
    [alertView show];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.contentTextView isFirstResponder] && [touch view] != self.contentTextView) {
        [self.contentTextView resignFirstResponder];
    }
    if ([self.titleTextField isFirstResponder] && [touch view] != self.titleTextField) {
        [self.titleTextField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
    
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
    
    

    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
	
	// set views with new info
    
    CGRect frame = self.contentView.frame;
    frame.size.height = height + 175.0f;
	self.contentView.frame = frame;
    
    self.scrollView.contentSize = self.contentView.bounds.size;
    
	
	// commit animations
	[UIView commitAnimations];
    
}

#pragma keyboardControl

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = self.scrollView.frame;
    containerFrame.size.height -= keyboardBounds.size.height;
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	self.scrollView.frame = containerFrame;
    
	
	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
    self.scrollView.frame = CGRectMake(0, 64.0f,
                                 PHONE_SCREEN_WIDTH, PHONE_SCREEN_HEIGHT-64.0f);
	
	
	// commit animations
	[UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        photoCnt = 0;
        self.photoImageArray = [[NSMutableArray alloc] init];
        self.photoImageDataArray = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollView.contentSize = self.contentView.bounds.size;
    self.scrollView.backgroundColor = [UIColor lightGrayColor];
    //self.scrollView.contentOffset = CGPointMake(0, 0);
    //self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    self.contentTextView.backgroundColor = [UIColor lightGrayColor];
    self.contentTextView.maxNumberOfLines = 1000;
    self.contentTextView.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.profileImageBtn setImageWithURL:[NSURL URLWithString:LMcommon.profile_mini_url]
                                 forState:UIControlStateNormal];
    self.nicknameLabel.text = LMcommon.nickname;

    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
