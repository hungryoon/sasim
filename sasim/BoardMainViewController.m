//
//  BoardMainViewController.m
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 21..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import "BoardMainViewController.h"
#import "BoardListTableViewCell.h"
#import "BoardWriteViewController.h"
#import "SVPullToRefresh.h"

#import "UIButton+WebCache.h"

@interface BoardMainViewController ()

@end

@implementation BoardMainViewController

- (IBAction)writeArticleBtnClicked:(id)sender {
    BoardWriteViewController *vc = [[BoardWriteViewController alloc] initWithNibName:@"BoardWriteViewController" bundle:nil];
    [vc addTarget:self action:@selector(loadFirstArticle)];
    vc.board_type = self.board_type;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.articleData = [[NSMutableArray alloc] init];
        
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    
    if (self.toRefresh) {
        [self loadFirstArticle];
        self.toRefresh = NO;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SWRevealViewController *revealController = self.revealViewController;
    [self.toggleBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    __weak BoardMainViewController *weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{ [weakSelf loadFirstArticle]; }];
    [self.tableView addInfiniteScrollingWithActionHandler:^{ [weakSelf loadNextArticle]; }];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.toRefresh = YES;
    
    //[self.tableView setSeparatorInset:UIEdgeInsetsZero];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)callback_loadArticle_first:(NSMutableDictionary *)result {
    
    NSLog(@"callback_loadArticle_first:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
   
    if ([[result objectForKey:@"result"] intValue] == 1) {
        NSDictionary *dict = [result objectForKey:@"values"];
        
        NSArray *callbackData = [NSArray arrayWithArray:[dict objectForKey:@"articles"]];
        
        
        self.articleData = [[NSMutableArray alloc] initWithArray:callbackData];
        
        [self.tableView reloadData];
        
        if ([callbackData count]==0) { // 더 이상 로드할 것이 없음
            isFinalArticle = YES;
            [self.tableView.infiniteScrollingView stopAnimating];
        }
    }
    
    [self.tableView.pullToRefreshView stopAnimating];
}

- (void)callback_loadArticle_next:(NSMutableDictionary *)result {
    
    NSLog(@"callback_loadArticle_next:%@", result);
    if (DEBUG_MODE) [LMcommon showMsgView:[result objectForKey:@"message"]];
    
    if ([[result objectForKey:@"result"] intValue] == 1) {
        
        NSDictionary *dict = [result objectForKey:@"values"];
        NSArray *callbackData = [NSArray arrayWithArray:[dict objectForKey:@"articles"]];
        
        
        [self.articleData addObjectsFromArray:callbackData];
        
        [self.tableView reloadData];
        
        if ([callbackData count]==0) { // 더 이상 로드할 것이 없음
            isFinalArticle = YES;
            [self.tableView.infiniteScrollingView stopAnimating];
        }
    }
    
    [self.tableView.infiniteScrollingView stopAnimating];
}

- (void)loadFirstArticle {
    
    isFinalArticle = NO;
    
    [LMapi api_loadArticle_member_id:LMcommon.member_id
                             session:LMcommon.session
                          board_type:self.board_type
                    start_article_id:nil
                              target:self
                              action:@selector(callback_loadArticle_first:)];

    
}

- (void)loadNextArticle {
    
    if (isFinalArticle) return;
    
    [LMapi api_loadArticle_member_id:LMcommon.member_id
                             session:LMcommon.session
                          board_type:self.board_type
                    start_article_id:[[self.articleData lastObject] objectForKey:@"article_id"]
                              target:self
                              action:@selector(callback_loadArticle_next:)];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // return 4;
     return [self.articleData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BoardListTableViewCell *cell;
    
    cell = (BoardListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BoardListTableViewCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BoardListTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib)
        {
            if ([oneObject isKindOfClass:[BoardListTableViewCell class]])
            {
                cell = (BoardListTableViewCell *)oneObject;
            }
        }
    }
    
    NSDictionary *article = [self.articleData objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [article objectForKey:@"title"];
    
    cell.nicknameLabel.text = [article objectForKey:@"nickname"];
    cell.hitCountLabel.text = [NSString stringWithFormat:@"%@", [article objectForKey:@"hit"]];
    cell.writtenTimeLabel.text = [article objectForKey:@"date_written"];
    
    cell.commentNumberLabel.text = [NSString stringWithFormat:@"%@", [article objectForKey:@"comment_number"]];
    
    
    NSString *profileImageURL = [NSString stringWithFormat:@"%@%@",
                                 BaseURL, [article objectForKey:@"micro_profile_image"]];
    
    [cell.profileBtn setImageWithURL:[NSURL URLWithString:profileImageURL]
                            forState:UIControlStateNormal];
    
    // Configure the cell...
    
    return cell;
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BoardDetailViewController *vc = [[BoardDetailViewController alloc] initWithNibName:@"BoardDetailViewController" bundle:nil];
    
    vc.article_id = [[self.articleData objectAtIndex:indexPath.row] objectForKey:@"article_id"];
    
    // boardDetailVC.article_id = @"1";
    
    [vc addTarget:self action:@selector(loadFirstArticle)];
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
