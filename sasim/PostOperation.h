//
//

#import <UIKit/UIKit.h>

@interface PostOperation : NSOperation
{
    NSDictionary    *dataDict;
    NSString        *aURLString;
    id              aTarget;
    SEL             aselector;
    
    NSMutableData *receiveData;
    
    BOOL connectionFinished;

}

@property(nonatomic, strong) NSDictionary    *dataDict;
@property(nonatomic, strong) NSString        *aURLString;
@property(nonatomic, strong) NSMutableData   *receiveData;


- (id)initWithUrlString:(NSString *)urlString     
                    dic:(NSDictionary *)dic 
                 target:(id)target 
                 action:(SEL)selector;

@end
