//
//  CommentFieldViewController.h
//  CommentField
//
//  Created by hungryoon on 2014. 5. 15..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@protocol CommentFieldDelegate

@optional
- (void)sendButtonClicked;
- (void)photoButtonClicked;
- (void)didChangeHeight;
@end


@interface CommentFieldViewController : UIViewController <HPGrowingTextViewDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    float imageWidth, imageHeight;
}


@property(unsafe_unretained) NSObject<CommentFieldDelegate> *delegate;

@property (nonatomic, strong) IBOutlet HPGrowingTextView *textView;
@property (nonatomic, strong) IBOutlet UIButton *photoButton;
@property (nonatomic, strong) IBOutlet UIButton *sendButton;

@property (nonatomic, strong) UIView *photoContainerView;
@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, strong) UIImage *photoImage;
@property (nonatomic, strong) NSData *photoImageData;





-(void)inputPhoto:(UIImage *)image;


@end
