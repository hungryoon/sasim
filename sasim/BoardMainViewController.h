//
//  BoardMainViewController.h
//  lionmind_ver_1_0_0
//
//  Created by hungryoon on 2014. 5. 21..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardDetailViewController.h"

@interface BoardMainViewController : UIViewController
{
    BOOL isFinalArticle;
}

@property (nonatomic) BOOL toRefresh;

@property (nonatomic, strong) NSString *board_type;
@property (nonatomic, strong) NSMutableArray  *articleData;

@property (nonatomic, strong) IBOutlet UITableView  *tableView;

@property (nonatomic, strong) IBOutlet UIButton  *toggleBtn;
@property (nonatomic, strong) IBOutlet UIButton  *naviTitleBtn;


- (IBAction)writeArticleBtnClicked:(id)sender;

@end
