//
//  FirstAgreeViewController.h
//  Agreement
//
//  Created by LeeJungWoo on 2014. 5. 22..
//  Copyright (c) 2014년 myro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstAgreeViewController : UIViewController
{
    NSInteger checkBoxFlag;
}

@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)checkBoxClicked:(id)sender;
- (IBAction)nextButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;

@end
