//
//  UIImage+fixOrientation.h
//  sasim
//
//  Created by hungryoon on 2014. 5. 31..
//  Copyright (c) 2014년 teamWSB. All rights reserved.
//

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end