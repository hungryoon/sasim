//
//  FirstAgreeViewController.m
//  Agreement
//
//  Created by LeeJungWoo on 2014. 5. 22..
//  Copyright (c) 2014년 myro. All rights reserved.
//

#import "FirstAgreeViewController.h"
#import "SecondAgreeViewController.h"

@interface FirstAgreeViewController ()

@end

@implementation FirstAgreeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nextButton.alpha = 0.0f;
    
    checkBoxFlag = 0;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkBoxClicked:(id)sender {


    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];

    if (checkBoxFlag) { // 체크 된 상태에서
        checkBoxFlag = 0;
        self.checkBoxButton.backgroundColor = [UIColor whiteColor];
    }
    else {
        checkBoxFlag = 1;
        self.checkBoxButton.backgroundColor = [UIColor blueColor];
    }

    // Next 띄워도 되는지
    if (checkBoxFlag)   self.nextButton.alpha = 1.0f;
    else                self.nextButton.alpha = 0.0f;
    
    
    [UIView commitAnimations];
    
}

- (IBAction)nextButtonClicked:(id)sender{
    SecondAgreeViewController *vc = [[SecondAgreeViewController alloc] initWithNibName:@"SecondAgreeViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
